<?php

namespace App\Modules\Providers;

use Illuminate\Support\ServiceProvider;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $modules = config('module.modules');
        $path = config('module.path');

        if (!empty($modules)) {
            /*перебираем группу модулей*/
            foreach ($modules as $groupModule => $subModules) {
                /*перебираем модули в группе*/
                foreach ($subModules as  $subModule) {
                    $nameModule = $groupModule.'.'.$subModule;
                    $relativePath = '/' . $groupModule . '/' .$subModule;
                    $routePath = $path . $relativePath . '/Routes/web.php';
                    $this->loadRoutes($routePath);
                    $this->loadViews($path, $relativePath, $nameModule);
                    $this->loadMigrations($routePath, $relativePath, $path);
                }
            }
        }
    }

    protected function loadRoutes($routePath)
    {
        if (file_exists($routePath)) {
            /*загружает маршруты, метод определяет кэшируються ли маршруты и не загружает если были кэшированы*/
            $this->loadRoutesFrom($routePath);
        }
    }

    protected function loadViews($path, $relativePath, $nameModule)
    {
        if (is_dir($path.'/'.$relativePath .'/Views')) {
            $this->loadViewsFrom($path.'/'.$relativePath.'/Views', $nameModule);
        }
    }

    protected function loadMigrations($routePath, $relativePath, $path)
    {
        if (is_dir($routePath.'/'.$relativePath.'/Migrations')) {
            $this->loadMigrationsFrom($path.'/'.$relativePath.'/Migrations');
        }
    }
}
