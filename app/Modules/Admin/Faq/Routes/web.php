<?php

use Illuminate\Support\Facades\Route;

use \App\Modules\Admin\Faq\Controllers\FaqController;

Route::prefix('admin')->group(function (){
    Route::prefix('faq')->group(function () {
        Route::get('index', [FaqController::class, 'index']);
    });
});

